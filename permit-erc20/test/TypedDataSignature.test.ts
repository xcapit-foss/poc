import { expect } from "chai";
import { ethers } from "hardhat";
import { Tokin } from "../typechain-types";
import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { Deadline } from "../models/Deadline";
import { PermitTypeValues } from "../models/PermitTypeValues";
import { EIP712Domain } from "../models/EIP712Domain";
import { PermitTypeParams } from "../models/PermitTypeParams";
import { DefaultTypedDataSignature, TypedDataSignature } from "../models/TypedDataSignature";


describe("DefaultTypedDataSignature", () => {
  let aTypedDataSignature: TypedDataSignature;

  async function deployOneYearLockFixture() {
    const anAmount = ethers.utils.parseEther("1");
    const [owner, otherAccount] = await ethers.getSigners();

    const TokinContract = await ethers.getContractFactory("Tokin");
    const tokin: Tokin = await TokinContract.deploy();

    return { tokin, owner, otherAccount, anAmount };
  }

  beforeEach(async () => {
    const deadline = new Deadline();
    const { tokin, owner, otherAccount, anAmount } = await loadFixture(
      deployOneYearLockFixture
    );
    aTypedDataSignature = new DefaultTypedDataSignature(
      new EIP712Domain(
        tokin,
        (await otherAccount.provider!.getNetwork()).chainId
      ),
      new PermitTypeParams(),
      new PermitTypeValues(
        tokin,
        owner,
        otherAccount.address,
        anAmount,
        deadline
      )
    );
  });

  it("new", async () => {
    expect(aTypedDataSignature).be.not.null;
  });

  it("value", async () => {
    expect(await aTypedDataSignature.value()).be.not.null;
  });
});
