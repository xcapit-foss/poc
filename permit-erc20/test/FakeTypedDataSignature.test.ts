import { expect } from 'chai';
import { FakeTypedDataSignature } from '../models/FakeTypedDataSignature';


describe('FakeTypedDataSignature', () => {
  it('new', () => {
    expect(new FakeTypedDataSignature());
  });

  it('value', async () => {
    const aSignature = new FakeTypedDataSignature();

    expect(await aSignature.value()).be.not.null;
  });
});
