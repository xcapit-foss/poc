import { loadFixture } from '@nomicfoundation/hardhat-network-helpers';
import { expect } from 'chai';
import { ethers } from 'hardhat';
import { Tokin } from '../typechain-types';
import { EIP712Domain } from '../models/EIP712Domain';
import { Deadline } from '../models/Deadline';
import { PermitTypeParams } from '../models/PermitTypeParams';
import { PermitTypeValues } from '../models/PermitTypeValues';
import { DefaultTypedDataSignature } from '../models/TypedDataSignature';
import { ComponentsOf } from '../models/ComponentsOf';

describe('Tokin', () => {
  async function deployOneYearLockFixture() {
    const anAmount = ethers.utils.parseEther('1');
    let randomAccount = ethers.Wallet.createRandom();
    const [owner, otherAccount] = await ethers.getSigners();
    randomAccount = randomAccount.connect(owner.provider!);

    const TokinContract = await ethers.getContractFactory('Tokin');
    const tokin: Tokin = await TokinContract.deploy();

    return { tokin, owner, otherAccount, randomAccount, anAmount };
  }

  it('deployed tokin', async () => {
    const { tokin } = await loadFixture(deployOneYearLockFixture);

    expect(tokin).be.not.null;
  });

  it('mint', async () => {
    const { tokin, otherAccount, anAmount } = await loadFixture(deployOneYearLockFixture);

    await tokin.mint(otherAccount.address, anAmount);

    expect(await tokin.balanceOf(otherAccount.address)).to.equal(anAmount);
  });

  it('random account with zero eth & zero tokin balance', async () => {
    const { tokin, randomAccount } = await loadFixture(deployOneYearLockFixture);

    expect(await randomAccount.getBalance()).to.equal(0);
    expect(await tokin.balanceOf(randomAccount.address)).to.equal(0);
  });

  it('permit', async () => {
    const { tokin, otherAccount, anAmount, randomAccount } = await loadFixture(deployOneYearLockFixture);
    await tokin.mint(randomAccount.address, anAmount);
    const aPermitTypeValues = new PermitTypeValues(tokin, randomAccount, otherAccount.address, anAmount, new Deadline(4200));
    const signatureComponents = new ComponentsOf(
      new DefaultTypedDataSignature(
        new EIP712Domain(tokin, (await otherAccount.provider!.getNetwork()).chainId),
        new PermitTypeParams(),
        aPermitTypeValues
      )
    );

    await tokin
      .connect(otherAccount)
      .permit(
        aPermitTypeValues.owner().address,
        aPermitTypeValues.spenderAddress(),
        aPermitTypeValues.amount(),
        aPermitTypeValues.deadline().toSeconds(),
        await signatureComponents.v(),
        await signatureComponents.r(),
        await signatureComponents.s()
      );

    await tokin.connect(otherAccount).transferFrom(randomAccount.address, otherAccount.address, anAmount);

    expect(await tokin.balanceOf(randomAccount.address)).to.equal(0);
    expect(await tokin.balanceOf(otherAccount.address)).to.equal(anAmount);
  });
});
