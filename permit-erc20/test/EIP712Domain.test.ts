import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";
import { Tokin } from "../typechain-types";
import { EIP712Domain } from "../models/EIP712Domain";


describe("EIP712Domain", () => {
  async function deployOneYearLockFixture() {
    const [owner] = await ethers.getSigners();
    const chainId = (await owner.provider!.getNetwork()).chainId;
    const TokinContract = await ethers.getContractFactory("Tokin");
    const tokin: Tokin = await TokinContract.deploy();

    return { tokin, chainId };
  }

  it("new", async () => {
    const { tokin, chainId } = await loadFixture(deployOneYearLockFixture);

    expect(new EIP712Domain(tokin, chainId)).be.not.null;
  });

  it("toJSON", async () => {
    const { tokin, chainId } = await loadFixture(deployOneYearLockFixture);
    const domain = new EIP712Domain(tokin, chainId);

    const aJSONValue = await domain.toJSON();

    expect(aJSONValue.version).to.equal('1');
    expect(aJSONValue.chainId).to.equal(31337);
    expect(aJSONValue.name).to.equal(await tokin.name());
    expect(aJSONValue.verifyingContract).to.equal(tokin.address);
  });
});
