import { expect } from "chai";
import { PermitTypeParams } from "../models/PermitTypeParams";


describe("PermitTypeParams", () => {
  it("new", async () => {
    expect(new PermitTypeParams([{}])).be.not.null;
  });

  it("new with default values", async () => {
    expect(new PermitTypeParams()).be.not.null;
  });

  it("value", async () => {
    const aPermitTypeParams = new PermitTypeParams();

    expect(aPermitTypeParams.value()).be.not.null;
    expect(aPermitTypeParams.value()['Permit']).be.not.null;
  });
});
