import { expect } from 'chai';
import { ethers } from 'hardhat';
import { Tokin } from '../typechain-types';
import { loadFixture } from '@nomicfoundation/hardhat-network-helpers';
import { Deadline } from '../models/Deadline';
import { PermitTypeValues } from '../models/PermitTypeValues';

describe('PermitTypeValues', () => {
  let aPermitTypeValues: PermitTypeValues;

  async function deployOneYearLockFixture() {
    const anAmount = ethers.utils.parseEther('1');
    const [owner, otherAccount] = await ethers.getSigners();

    const TokinContract = await ethers.getContractFactory('Tokin');
    const tokin: Tokin = await TokinContract.deploy();

    return { tokin, owner, otherAccount, anAmount };
  }

  beforeEach(async () => {
    const { tokin, owner, otherAccount, anAmount } = await loadFixture(deployOneYearLockFixture);
    aPermitTypeValues = new PermitTypeValues(tokin, owner, otherAccount.address, anAmount, new Deadline());
  });

  it('new', async () => {
    expect(aPermitTypeValues).be.not.null;
  });

  it('value', async () => {
    expect(await aPermitTypeValues.value()).be.not.null;
  });

  it('owner', async () => {
    expect(await aPermitTypeValues.owner()).be.not.null;
  });

  it('spenderAddress', async () => {
    const { otherAccount } = await loadFixture(deployOneYearLockFixture);

    expect(aPermitTypeValues.spenderAddress()).equal(otherAccount.address);
  });

  it('amount', async () => {
    const { anAmount } = await loadFixture(deployOneYearLockFixture);

    expect(aPermitTypeValues.amount()).equal(anAmount);
  });

  it('deadline', async () => {
    expect(aPermitTypeValues.deadline().toSeconds()).be.not.null;
  });
});
