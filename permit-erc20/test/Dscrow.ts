import { loadFixture } from '@nomicfoundation/hardhat-network-helpers';
import { expect } from 'chai';
import { ethers } from 'hardhat';
import { Dscrow, Tokin } from '../typechain-types';
import { EIP712Domain } from '../models/EIP712Domain';
import { Deadline } from '../models/Deadline';
import { PermitTypeParams } from '../models/PermitTypeParams';
import { PermitTypeValues } from '../models/PermitTypeValues';
import { DefaultTypedDataSignature } from '../models/TypedDataSignature';
import { ComponentsOf } from '../models/ComponentsOf';

describe('Dscrow', () => {
  async function deployOneYearLockFixture() {
    const anAmount = ethers.utils.parseEther('1');
    let randomAccount = ethers.Wallet.createRandom();
    const [owner, otherAccount] = await ethers.getSigners();
    randomAccount = randomAccount.connect(owner.provider!);

    const TokinContract = await ethers.getContractFactory('Tokin');
    const tokin: Tokin = await TokinContract.deploy();
    const DscrowContract = await ethers.getContractFactory('Dscrow');
    const dscrow: Dscrow = await DscrowContract.deploy(tokin.address);

    return { tokin, dscrow, owner, otherAccount, randomAccount, anAmount };
  }

  it('deploy', async () => {
    const { tokin, dscrow } = await loadFixture(deployOneYearLockFixture);

    expect(tokin).be.not.null;
    expect(dscrow).be.not.null;
  });

  it('depositWithPermit, the same address have Tokin balance & ETH balance', async () => {
    const { tokin, dscrow, anAmount, otherAccount } = await loadFixture(deployOneYearLockFixture);
    await tokin.mint(otherAccount.address, anAmount);
    const aPermitTypeValues = new PermitTypeValues(tokin, otherAccount, dscrow.address, anAmount, new Deadline(4200));
    const signatureComponents = new ComponentsOf(
      new DefaultTypedDataSignature(
        new EIP712Domain(tokin, (await otherAccount.provider!.getNetwork()).chainId),
        new PermitTypeParams(),
        aPermitTypeValues
      )
    );

    await dscrow
      .connect(otherAccount)
      .depositWithPermit(
        aPermitTypeValues.owner().address,
        aPermitTypeValues.amount(),
        aPermitTypeValues.deadline().toSeconds(),
        await signatureComponents.v(),
        await signatureComponents.r(),
        await signatureComponents.s()
      );

    expect(await tokin.balanceOf(otherAccount.address)).equal(0);
    expect(await tokin.balanceOf(dscrow.address)).equal(anAmount);
  });

  it('depositWithPermit, one address have Tokin balance & other ETH balance', async () => {
    const { tokin, dscrow, anAmount, otherAccount, randomAccount } = await loadFixture(deployOneYearLockFixture);
    await tokin.mint(randomAccount.address, anAmount);
    const aPermitTypeValues = new PermitTypeValues(tokin, randomAccount, dscrow.address, anAmount, new Deadline(4200));
    const signatureComponents = new ComponentsOf(
      new DefaultTypedDataSignature(
        new EIP712Domain(tokin, (await randomAccount.provider!.getNetwork()).chainId),
        new PermitTypeParams(),
        aPermitTypeValues
      )
    );

    await dscrow
      .connect(otherAccount)
      .depositWithPermit(
        aPermitTypeValues.owner().address,
        aPermitTypeValues.amount(),
        aPermitTypeValues.deadline().toSeconds(),
        await signatureComponents.v(),
        await signatureComponents.r(),
        await signatureComponents.s()
      );

    expect(await tokin.balanceOf(otherAccount.address)).equal(0);
    expect(await tokin.balanceOf(randomAccount.address)).equal(0);
    expect(await tokin.balanceOf(dscrow.address)).equal(anAmount);
  });
});
