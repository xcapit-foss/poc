import { expect } from "chai";
import { Deadline } from "../models/Deadline";


describe("DeadlineIn", () => {
  it("new", async () => {
    expect(new Deadline(60, new Date())).be.not.null;
  });

  it("new with default values", async () => {
    expect(new Deadline()).be.not.null;
  });

  it("timestamp in seconds", async () => {
    const seconds = 70;
    const aDate = new Date("2023/01/01");
    const aTimestampInSecondsDate = 1672542000;
    const deadline = new Deadline(seconds, aDate);

    expect(deadline.toSeconds()).be.equal(
      aTimestampInSecondsDate + 70
    );
    expect(deadline.toSeconds()).be.greaterThan(
      aTimestampInSecondsDate
    );
  });
});
