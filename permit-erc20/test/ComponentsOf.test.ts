import { expect } from 'chai';
import { ComponentsOf } from '../models/ComponentsOf';
import { TypedDataSignature } from '../models/TypedDataSignature';
import { FakeTypedDataSignature } from '../models/FakeTypedDataSignature';


describe('ComponentsOf', () => {
  const aSignature: TypedDataSignature = new FakeTypedDataSignature();
  let signatureComponents: ComponentsOf;

  beforeEach(() => {
    signatureComponents = new ComponentsOf(aSignature);
  });

  it('new', () => {
    expect(signatureComponents).be.not.null;
  });

  it('v, r, s components', async () => {
    expect(await signatureComponents.v()).be.not.null;
    expect(await signatureComponents.r()).be.not.null;
    expect(await signatureComponents.s()).be.not.null;
  });
});
