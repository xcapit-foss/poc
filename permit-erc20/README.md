# Permit (EIP-2612)

En la interacción con tokens ERC-20 y protocolos que los utilizan, siempre hay dos situaciones que generan fricción en el usuario.
* La necesidad de tener balance en ETH (o la crypto de la blockchain en cuestión) para poder realizar un envío de un token ERC-20.
* La necesidad de realizar 2 transacciones (``approve``, ``protocol tx``) para interactuar con un protocolo que use un token ERC-20.
Con el objetivo de mitigar estas fricciones es que se crea el [EIP-2612](https://eips.ethereum.org/EIPS/eip-2612) que propone una forma estandarizada para que los usuarios concedan permiso a otros para gastar sus tokens en una única transacción, simplificando el proceso de aprobación de transferencias de tokens. También habilita la transferencia de tokens gasless, o sea, sin tener que pagar la fee de la transacción.

## Standard Allowance Model
El famoso ``approve + interaction``, 2 transactions.

![erc20 transferFrom flow](./assets/erc20-transferFrom.png)
## Permit (EIP-2612) Model
Usando ``permit``, solo ``interaction``, 1 transaction.

![erc2612 permit flow](./assets/erc2612-permit.png)

Ej. básico del flow.

![erc2612 permit flow 2](./assets/permit-flow.png)
### Como funciona?
Tanto en el frontend como del lado de los smart contracts, se hace uso del [EIP-712 Signed Messages](https://eips.ethereum.org/EIPS/eip-712) para obtener y verificar un *human-readeable* signed message que contiene los detalles del permit. Un contrato debería aceptar este mensaje firmado como parámetro adicional y cuando sea necesario pasarlo al method ``permit`` del contrato del token, consediendose a si mismo el allowance, que debería ser inmediatamente (en la misma transacción) utilizado.
### The Permit Message
El mensaje permite que el user tiene que firmar (off-chain) tiene 5 campos:
| name | type | meaning |
|------|------|---------|
| `owner` | `address` | owner of the tokens (the user) |
| `spender` | `address` | who is being granted an allowance (the protocol) |
| `value` | `uint256` | how much `spender` can transfer from `owner` |
| `nonce` | `uint256` | an on-chain incrementing value unique to `owner` that must match for this message to be valid (see below) |
|  `deadline` | `uint256` | timestamp after which this message expires |

Un ejemplo de como armar el mensaje, firmarlo y enviarlo a un contrato de token ERC-20 que lo soporte se puede encontrar en el [test de permit](./test/Tokin.ts#L46). En este caso usamos un par de objetos para facilitar la lectura del código, y la lib [ethers js](https://docs.ethers.org/v5/).
En el test, utilizamos 2 wallets, la wallet A tiene tokens, pero no tiene ETH, y la wallet B tiene ETH pero no tokens. En este caso la wallet A firma off-chain el mensaje para el ``permit`` con los detalles necesarios para darle a la wallet B el permiso para poder enviar sus tokens (ejecutar un ``transferFrom(A, B, anAmount)``). Ahora la wallet B envía el mensaje firmado de la wallet A, y luego transfiere los tokens.

La integración en un smart contract es bastante simple, y es la idea que se ejemplifica en el diagrama de flow. Se puede encontrar un ejemplo de esto [aquí](./contracts/Dscrow.sol#L14) y los tests que muestran algunas de las posibles interacciones con el contrato, [depositWithPermit, the same address have Tokin balance & ETH balance](./test/Dscrow.ts#L34) y [depositWithPermit, one address have Tokin balance & other ETH balance](./test/Dscrow.ts#L61). 

## Compatibilidad
Como ``permit`` se propuso después de que muchos tokens ERC-20 fueran deployados, no todos lo implementan, y hasta algunos tokens nuevos tampoco lo implementan. Hay casos en que la implementación difiere del [EIP-2612](https://eips.ethereum.org/EIPS/eip-2612), esto pasa por ejemplo con DAI ([DAI Permit implementation](https://docs.makerdao.com/smart-contract-modules/dai-module/dai-detailed-documentation#3.-key-mechanisms-and-concepts)). En el caso de USDC, [USDC Contract in Polygon](https://polygonscan.com/token/0x2791bca1f2de4661ed88a30c99a7a9449aa84174#readProxyContract), si implementa ``permit`` siguiendo el [EIP-2612](https://eips.ethereum.org/EIPS/eip-2612).

A pesar de lo anterior, ``permit`` como lo propone [EIP-2612](https://eips.ethereum.org/EIPS/eip-2612), es implementado por muchos tokens.

En [este repo](https://github.com/yashnaman/tokensWithPermitFunctionList) se puede encontrar una lista de tokens que implementan ``permit`` ala [EIP-2612](https://eips.ethereum.org/EIPS/eip-2612) way y otra ala [DAI Permit implementation](https://docs.makerdao.com/smart-contract-modules/dai-module/dai-detailed-documentation#3.-key-mechanisms-and-concepts) way. El repo esta un poco desactualizado, por lo que es super recomendable buscar el contrato del token en el scan e inspeccionar como implementa ``permit`` si es que lo implementa.

Hay formas de utilizar ``permit`` en tokens que no lo implementen. Uniswap penso en esto y lanzo [Permit2](https://docs.uniswap.org/contracts/permit2/overview), aunque no es el objetivo de este spike hablar de esto, creo que esta bueno comentarlo.

## Conclusiones
El ``permit`` habilita una forma de ayanar el camino a las gasless transactions como también a mejorar la UX de las client apps al reducir el nro de transacciones necesarias para interactuar con un protocolo, siempre y cuando el protocolo implemente ``permit``.


Es válido también tener en cuenta los posibles riesgos de phishing que el ``permit`` trae asociado. Sobre este tema es muy interesante el [post de Medium - The Dark Side of Permits](https://medium.com/@romanrakhlin/the-dark-side-of-permits-eip2612-c66ff71bf635).
Sobre este tema, cabe destacar que es muy importante trabajar para mejorar la comunicación con el user, para que este sepa que esta haciendo/que esta firmando con su wallet, en todo momento.


## Links
* [EIP-2612](https://eips.ethereum.org/EIPS/eip-2612)
* [EIP-712](https://eips.ethereum.org/EIPS/eip-712)
* [OpenZeppelin ERC20 Permit](https://docs.openzeppelin.com/contracts/4.x/api/token/erc20#ERC20Permit)
* [QuickNode - How To Approve Token Transfers Without Spending Gas Using EIP-2612](https://www.quicknode.com/guides/ethereum-development/transactions/how-to-use-erc20-permit-approval/#creating-the-deployment-and-interaction-script)
* [Solidity Developer - A Long Way To Go: On Gasless Tokens and ERC20-Permit](https://soliditydeveloper.com/erc20-permit)
* [1inch - Permit 712 signed token approvals and how the work](https://help.1inch.io/en/articles/5435386-permit-712-signed-token-approvals-and-how-they-work-on-1inch)
* [Twitter - ERC20 Permit Phishing](https://twitter.com/realScamSniffer/status/1639260170021740545?lang=es)
* [Medium - The Dark Side of Permits](https://medium.com/@romanrakhlin/the-dark-side-of-permits-eip2612-c66ff71bf635)
* [DAI Permit Docs](https://docs.makerdao.com/smart-contract-modules/dai-module/dai-detailed-documentation#3.-key-mechanisms-and-concepts)
* [Uniswap Permit](https://github.com/Uniswap/v2-core/blob/master/contracts/UniswapV2ERC20.sol)
* [Uniswap Permit2](https://docs.uniswap.org/contracts/permit2/overview)
* [ERC-20 Permit, repo with examples](https://github.com/dragonfly-xyz/useful-solidity-patterns/tree/main/patterns/erc20-permit)