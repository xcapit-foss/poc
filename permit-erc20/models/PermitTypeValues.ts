import { Deadline } from './Deadline';
import { Tokin } from '../typechain-types';

export class PermitTypeValues {
  constructor(
    private _tokin: Tokin,
    private _owner: any,
    private _spenderAddress: string,
    private _anAmount: any,
    private _aDeadline: Deadline
  ) {}

  async value(): Promise<any> {
    return {
      owner: this._owner.address,
      spender: this._spenderAddress,
      value: this._anAmount,
      nonce: await this._tokin.nonces(this._owner.address),
      deadline: this._aDeadline.toSeconds(),
    };
  }

  owner(): any {
    return this._owner;
  }

  deadline(): Deadline{
    return this._aDeadline;
  }

  amount(): any {
    return this._anAmount;
  }

  spenderAddress(): string {
    return this._spenderAddress;
  }
}
