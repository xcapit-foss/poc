import { ethers } from "hardhat";
import { TypedDataSignature } from '../models/TypedDataSignature';


export class ComponentsOf {
  private _cachedSignatureValue: any;

  constructor(private _aSignature: TypedDataSignature) {}

  async v(): Promise<string> {
    return (await this._signatureValue()).v;
  }

  async r(): Promise<string> {
    return (await this._signatureValue()).r;
  }

  async s(): Promise<string> {
    return (await this._signatureValue()).s;
  }

  private async _signatureValue(): Promise<any> {
    if (!this._cachedSignatureValue) {
      this._cachedSignatureValue = ethers.utils.splitSignature(await this._aSignature.value());
    }
    return this._cachedSignatureValue;
  }
}