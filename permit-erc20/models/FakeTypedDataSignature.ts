import { TypedDataSignature } from "./TypedDataSignature";


export class FakeTypedDataSignature implements TypedDataSignature {

  async value(): Promise<string> {
    return '0xa236bf240eab490a3d6c1d7dcabc17157b8f0b53d65a9f27869676da067d27113980d7e84928576a29bd1c4a7deeb2aa38ba712c361cadf6c6e67846b22ab51b1b';
  }
}