// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";


contract Dscrow { 
	address _anERC20Token;

    constructor(address anERC20Token) {
		_anERC20Token = anERC20Token;
	}

	function depositWithPermit(
		address anOwner,
		uint256 anAmount,
		uint256 aDeadline,
        uint8 v,
        bytes32 r,
        bytes32 s
	) external {
		SafeERC20.safePermit(
			IERC20Permit(_anERC20Token),
			anOwner,
			address(this),
			anAmount,
			aDeadline,
			v,r,s	
		);
		SafeERC20.safeTransferFrom(IERC20(_anERC20Token), anOwner, address(this), anAmount);
	}
}
