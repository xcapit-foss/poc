# TODO
- [x] Deposit
- [x] Withdraw
- [x] Shares
- [x] Basic Controlls
- [x] AAVE Integration
- [x] ???? & ....

# Xearn
[Xearn](./contracts/Xearn.sol) es la primera prueba de concepto de un smart contract que recibe depositos, emite los shares correspondientes a esos depositos y los envía a una pool de [AAVE](https://aave.com/). El contrato también permite hacer withdraw de los depositos. 

## Deposit
El contrato recive un amount de un token ERC20, emite las shares correspondientes a ese amount y lo transfiere a una pool de [AAVE](https://aave.com/). 

## Withdraw
El contrato recive una cantidad de shares, estas se usan para calcular el amount de token ERC20 a retirar y son *quemadas* para mantener la proporcionalidad entre la cantidad del token ERC20 y la cantidad de shares emitidas. 

## Shares
Representan la participación de un usuario en el contrato [Xearn](./contracts/Xearn.sol) en relación con el saldo total de tokens y la cantidad total de shares en circulación. Mantienen un equilibrio justo y proporcionado entre los titulares en función de sus contribuciones al contrato.

## Basic Controlls
Se realizan algunos controles, en donde uno de ellos que puede ser un poco especial y vale la pena mencionar es `withHealthyPool`, en donde controlamos el precio de la share para saber si la pool esta en un estado en donde no es convenente depositar.

## ???? & ....
* Es necesario permitir la transferencia de shares en el dominio del [Xscrow](https://gitlab.com/xcapit-foss/xscrow)? Creo que no, las transferencias no deberían ser permitidas. 
* Es necesario agregar un control al límite de la Pool? (Pool Deposit Limit).
* Como esto es solo una prueba de concepto, faltan controles y funcionalidades.
	* onlyXscrow modifier (en el caso de usarse en el domonio del [Xscrow](https://gitlab.com/xcapit-foss/xscrow))
	* modificar los métodos que usan `msg.sender`, ya que si lo usamos con el [Xscrow](https://gitlab.com/xcapit-foss/xscrow) necesitamos pasar la address del user.
	* Una forma de `supplyAll` en respuesta al `withdrawAll` existente.
	* Una forma de *migrar* o otro contrato *Xearn*, tal vez con otra estrategia?
	* Separar el token share ERC20 de [Xearn](./contracts/Xearn.sol)?
	* Agregar controles de Reentrancy.
	* Definir los eventos a emitir.
	* Definir pausa/stop/etc..
	* Check de la existencia de la Pool en [AAVE](https://aave.com/).?

# Links
* [Vault Math](https://www.youtube.com/watch?v=k7WNibJOBXE)
* [2pi Contracts](https://github.com/2pinetwork/contracts/tree/master)
* [AAVE Contracts](https://github.com/aave/aave-v3-core)
* [AAVE Documentation](https://docs.aave.com/developers/getting-started/readme)