// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import {IPool} from "@aave/core-v3/contracts/interfaces/IPool.sol";
import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {IAToken} from "@aave/core-v3/contracts/interfaces/IAToken.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol";
import {IPoolDataProvider} from "@aave/core-v3/contracts/interfaces/IPoolDataProvider.sol";
import {IPoolAddressesProvider} from "@aave/core-v3/contracts/interfaces/IPoolAddressesProvider.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";

contract Xearn is ERC20, Ownable {
    uint256 public constant RATIO_PRECISION = 10000;
    using SafeERC20 for IERC20Metadata;
    IERC20Metadata _aToken;
    IPoolDataProvider _aPoolDataProvider;
    IPoolAddressesProvider _aPoolAddressProvider;
    uint256 withdrawShareThreshold;
    uint256 depositShareThreshold;

    constructor(
        IERC20Metadata aToken_,
        IPoolDataProvider aPoolDataProvider_,
        IPoolAddressesProvider aPoolAddressProvider_
    ) ERC20("xcptDAI", "xcptDAI") {
        _aToken = aToken_;
        _aPoolDataProvider = aPoolDataProvider_;
        _aPoolAddressProvider = aPoolAddressProvider_;
        withdrawShareThreshold =
            ((10 ** _aToken.decimals()) * 9900) /
            RATIO_PRECISION;
        depositShareThreshold =
            ((10 ** _aToken.decimals()) * 9900) /
            RATIO_PRECISION;
    }

    modifier withHealthyPool(uint256 threshold) {
        require(sharePrice() >= threshold, "Unhealthy Pool Before Transfer");
        _;
        require(sharePrice() >= threshold, "Unhealthy Pool After Transfer");
    }

    modifier nonZeroValue(uint256 value) {
        require(value > 0, "Non zero value");
        _;
    }

    function deposit(
        uint256 amount_
    ) public nonZeroValue(amount_) withHealthyPool(depositShareThreshold) {
        uint256 balanceBeforeTransfer = totalTokenBalance();
        _aToken.transferFrom(msg.sender, address(this), amount_);
        _mint(msg.sender, _shares(balanceBeforeTransfer));
        _aToken.approve(address(_pool()), amount_);
        _pool().supply(address(_aToken), amount_, address(this), 0);
    }

    function withdraw(
        uint256 shares_
    ) public nonZeroValue(shares_) withHealthyPool(withdrawShareThreshold) {
        require(
            balanceOf(msg.sender) >= shares_,
            "Insufficient shares to withdraw"
        );
        uint256 amountToWithdraw = tokenAmountOf(shares_);
        _burn(msg.sender, shares_);
        uint256 balanceBeforeWithdraw = _tokenBalance();

        if (balanceBeforeWithdraw < amountToWithdraw) {
            _withdrawFromPool(amountToWithdraw);

            if (_tokenBalance() < amountToWithdraw) {
                amountToWithdraw = _tokenBalance();
            }
        }

        _aToken.safeTransfer(msg.sender, amountToWithdraw);
    }

    function withdrawAll() external onlyOwner {
        _pool().withdraw(address(_aToken), type(uint).max, address(this));
    }

    function sharePrice() public view returns (uint) {
        uint _totalSupply = totalSupply();
        uint _precision = 10 ** decimals();
        return
            _totalSupply <= 0
                ? _precision
                : ((totalTokenBalance() * _precision) / _totalSupply);
    }

    function totalTokenBalance() public view returns (uint) {
        return _tokenBalance() + _balanceOfPool();
    }

    function tokenAmountOf(uint256 shares) public view returns (uint256) {
        return (totalTokenBalance() * shares) / totalSupply();
    }

    function _withdrawFromPool(uint256 amountToWithdraw_) private {
        uint256 balanceBeforeWithdraw = _tokenBalance();
        uint256 amountToWithdrawFromPool = amountToWithdraw_ -
            balanceBeforeWithdraw;
        _pool().withdraw(
            address(_aToken),
            amountToWithdrawFromPool,
            address(this)
        );
        require(
            (_tokenBalance() - balanceBeforeWithdraw) > 0,
            "Can't withdraw from strategy..."
        );
    }

    function _tokenBalance() private view returns (uint) {
        return _aToken.balanceOf(address(this));
    }

    function _balanceOfPool() private view returns (uint) {
        (uint _supplyBal, , uint _borrowBal, , , , , , ) = _aPoolDataProvider
            .getUserReserveData(address(_aToken), address(this));
        return _supplyBal - _borrowBal;
    }

    function _pool() private view returns (IPool) {
        return IPool(_aPoolAddressProvider.getPool());
    }

    function _shares(
        uint256 balanceBeforeTransfer
    ) private view returns (uint256) {
        uint256 balanceDiffAfterTransfer = totalTokenBalance() -
            balanceBeforeTransfer;
        uint256 shares = balanceDiffAfterTransfer;
        if (totalSupply() > 0) {
            shares =
                (balanceDiffAfterTransfer * totalSupply()) /
                balanceBeforeTransfer;
        }

        return shares;
    }
}
