// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import {IPoolDataProvider} from "@aave/core-v3/contracts/interfaces/IPoolDataProvider.sol";
import {IPoolAddressesProvider} from "@aave/core-v3/contracts/interfaces/IPoolAddressesProvider.sol";
import {IPool} from "@aave/core-v3/contracts/interfaces/IPool.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract FakePoolDataProvider is IPoolDataProvider {
    IPool _pool;

    constructor(IPool aPool_) {
        _pool = aPool_;
    }

    function getUserReserveData(
        address asset,
        address user
    )
        public
        view
        returns (
            uint256 currentATokenBalance,
            uint256 currentStableDebt,
            uint256 currentVariableDebt,
            uint256 principalStableDebt,
            uint256 scaledVariableDebt,
            uint256 stableBorrowRate,
            uint256 liquidityRate,
            uint40 stableRateLastUpdated,
            bool usageAsCollateralEnabled
        )
    {
        require(asset != address(0x0), "");
        require(user != address(0x0), "");
        return (
            IERC20(asset).balanceOf(address(_pool)),
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            false
        );
    }

    function ADDRESSES_PROVIDER()
        external
        view
        override
        returns (IPoolAddressesProvider)
    {}

    function getAllReservesTokens()
        external
        view
        override
        returns (TokenData[] memory)
    {}

    function getAllATokens()
        external
        view
        override
        returns (TokenData[] memory)
    {}

    function getReserveConfigurationData(
        address asset
    )
        external
        view
        override
        returns (
            uint256 decimals,
            uint256 ltv,
            uint256 liquidationThreshold,
            uint256 liquidationBonus,
            uint256 reserveFactor,
            bool usageAsCollateralEnabled,
            bool borrowingEnabled,
            bool stableBorrowRateEnabled,
            bool isActive,
            bool isFrozen
        )
    {}

    function getReserveEModeCategory(
        address asset
    ) external view override returns (uint256) {}

    function getReserveCaps(
        address asset
    ) external view override returns (uint256 borrowCap, uint256 supplyCap) {}

    function getPaused(
        address asset
    ) external view override returns (bool isPaused) {}

    function getSiloedBorrowing(
        address asset
    ) external view override returns (bool) {}

    function getLiquidationProtocolFee(
        address asset
    ) external view override returns (uint256) {}

    function getUnbackedMintCap(
        address asset
    ) external view override returns (uint256) {}

    function getDebtCeiling(
        address asset
    ) external view override returns (uint256) {}

    function getDebtCeilingDecimals()
        external
        pure
        override
        returns (uint256)
    {}

    function getReserveData(
        address asset
    )
        external
        view
        override
        returns (
            uint256 unbacked,
            uint256 accruedToTreasuryScaled,
            uint256 totalAToken,
            uint256 totalStableDebt,
            uint256 totalVariableDebt,
            uint256 liquidityRate,
            uint256 variableBorrowRate,
            uint256 stableBorrowRate,
            uint256 averageStableBorrowRate,
            uint256 liquidityIndex,
            uint256 variableBorrowIndex,
            uint40 lastUpdateTimestamp
        )
    {}

    function getATokenTotalSupply(
        address asset
    ) external view override returns (uint256) {}

    function getTotalDebt(
        address asset
    ) external view override returns (uint256) {}

    function getReserveTokensAddresses(
        address asset
    )
        external
        view
        override
        returns (
            address aTokenAddress,
            address stableDebtTokenAddress,
            address variableDebtTokenAddress
        )
    {}

    function getInterestRateStrategyAddress(
        address asset
    ) external view override returns (address irStrategyAddress) {}

    function getFlashLoanEnabled(
        address asset
    ) external view override returns (bool) {}
}
