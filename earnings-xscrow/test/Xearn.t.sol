// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "forge-std/Test.sol";
import "../contracts/Xearn.sol";
import "../contracts/fakes/FakeToken.sol";
import "../contracts/fakes/FakePoolDataProvider.sol";
import "../contracts/fakes/FakePoolAddressesProvider.sol";

contract XearnTest is Test {
    Xearn xearn;
    FakeToken fakeToken;
    FakePoolAddressesProvider fakePoolAddressesProvider;
    address wallet1 = address(1);
    address wallet2 = address(2);
    uint testAmount = 1 ether;

    function setUp() public {
        fakeToken = new FakeToken("DAI", "DAI");
        fakePoolAddressesProvider = new FakePoolAddressesProvider();

        xearn = new Xearn(
            fakeToken,
            new FakePoolDataProvider(
                IPool(fakePoolAddressesProvider.getPool())
            ),
            fakePoolAddressesProvider
        );
        fakeToken.mint(wallet1, 2 ether);
        fakeToken.mint(wallet2, 2 ether);
    }

    function test_deposit() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount);

        xearn.deposit(testAmount);

        assertEq(xearn.totalTokenBalance(), testAmount);
        assertEq(xearn.balanceOf(wallet1), testAmount);
        assertEq(xearn.balanceOf(wallet1), xearn.totalSupply());
    }

    function test_depositWithoutUserAllowance() public {
        vm.startPrank(wallet1);

        vm.expectRevert("ERC20: insufficient allowance");
        xearn.deposit(testAmount);
    }

    function test_multiDeposit() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount * 2);

        xearn.deposit(testAmount);
        xearn.deposit(testAmount);

        assertEq(xearn.totalTokenBalance(), testAmount * 2);
        assertEq(xearn.balanceOf(wallet1), testAmount * 2);
        assertEq(xearn.balanceOf(wallet1), xearn.totalSupply());
    }

    function test_withdraw() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount);
        xearn.deposit(testAmount);

        xearn.withdraw(testAmount);

        assertEq(xearn.totalTokenBalance(), 0);
        assertEq(xearn.totalSupply(), 0);
        assertEq(xearn.balanceOf(wallet1), 0);
        assertEq(fakeToken.balanceOf(wallet1), 2 ether);
    }

    function test_withdrawZeroShares() public {
        vm.startPrank(wallet1);
        vm.expectRevert("Non zero value");
        xearn.withdraw(0);
    }

    function test_withdrawWithEmptyPool() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount);
        xearn.deposit(testAmount);

        vm.stopPrank();
        xearn.withdrawAll();

        vm.prank(wallet1);
        xearn.withdraw(testAmount);

        assertEq(xearn.totalTokenBalance(), 0);
        assertEq(xearn.totalSupply(), 0);
        assertEq(xearn.balanceOf(wallet1), 0);
        assertEq(fakeToken.balanceOf(wallet1), 2 ether);
    }

    function test_withdrawWithWeirdPool() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount);
        xearn.deposit(testAmount);
        FakePool(fakePoolAddressesProvider.getPool()).pause(true);

        vm.expectRevert("Can't withdraw from strategy...");
        xearn.withdraw(testAmount);
    }

    function test_withdrawWrongAmount() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount);
        xearn.deposit(testAmount);

        FakePool(fakePoolAddressesProvider.getPool()).returnLess(true);

        xearn.withdraw(testAmount);

        assertEq(xearn.totalTokenBalance(), 0.8 ether);
        assertEq(xearn.totalSupply(), 0);
        assertEq(xearn.balanceOf(wallet1), 0);
        assertEq(fakeToken.balanceOf(wallet1), 1.2 ether);
    }

    function test_withdrawMoreSharesThanIHave() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount);
        xearn.deposit(testAmount);

        vm.expectRevert("Insufficient shares to withdraw");
        xearn.withdraw(testAmount * 2);
    }

    function test_withdrawWithoutShares() public {
        vm.startPrank(wallet1);

        vm.expectRevert("Insufficient shares to withdraw");
        xearn.withdraw(testAmount);
    }

    function test_sharePrice() public {
        assertEq(xearn.sharePrice(), 10 ** 18);
    }

    function test_sharePriceWithDeposit() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount * 2);
        xearn.deposit(testAmount);
        xearn.deposit(testAmount);
        assertEq(xearn.sharePrice(), 10 ** 18);
    }

    function test_whitdrawWithUnhealthyPoolBeforeTransfer() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount);
        xearn.deposit(testAmount);
        vm.stopPrank();

        vm.prank(fakePoolAddressesProvider.getPool());
        fakeToken.transfer(wallet2, testAmount / 2);

        vm.expectRevert("Unhealthy Pool Before Transfer");
        vm.prank(wallet1);
        xearn.withdraw(testAmount);
    }

    function test_whitdrawWithUnhealthyPoolAfterTransfer() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount * 2);
        xearn.deposit(testAmount);
        xearn.deposit(testAmount);

        FakePool(fakePoolAddressesProvider.getPool()).lowSharePrice(true);

        vm.expectRevert("Unhealthy Pool After Transfer");
        xearn.withdraw(testAmount);
    }

    function test_depositZeroAmount() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount);

        vm.expectRevert("Non zero value");
        xearn.deposit(0);
    }

    function test_depositWithUnhealthyPoolBeforeTransfer() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount * 2);
        xearn.deposit(testAmount);
        vm.stopPrank();

        vm.prank(fakePoolAddressesProvider.getPool());
        fakeToken.transfer(wallet2, testAmount / 2);

        vm.expectRevert("Unhealthy Pool Before Transfer");
        vm.prank(wallet1);
        xearn.deposit(testAmount);
    }

    function test_depositWithUnhealthyPoolAfterTransfer() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount * 2);
        xearn.deposit(0.89 ether);
        xearn.deposit(0.6 ether);

        FakePool(fakePoolAddressesProvider.getPool()).lowSharePrice(true);

        vm.expectRevert("Unhealthy Pool After Transfer");
        xearn.deposit(0.3 ether);
    }

    function test_userBalance() public {
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount);
        xearn.deposit(0.89 ether);
        vm.stopPrank();

        vm.startPrank(wallet2);
        fakeToken.approve(address(xearn), testAmount);
        xearn.deposit(0.27 ether);

        assertEq(
            xearn.tokenAmountOf(xearn.balanceOf(wallet1)),
            (xearn.sharePrice() * xearn.balanceOf(wallet1)) /
                (10 ** xearn.decimals())
        );
    }

    function test_withdrawAllNotOwner() public {
        fakeToken.mint(wallet1, testAmount);
        vm.startPrank(wallet1);
        fakeToken.approve(address(xearn), testAmount);
        xearn.deposit(testAmount);

        vm.expectRevert("Ownable: caller is not the owner");
        xearn.withdrawAll();
    }
}
