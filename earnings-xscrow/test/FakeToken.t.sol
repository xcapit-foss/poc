// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "forge-std/Test.sol";
import "../contracts/fakes/FakeToken.sol";

contract FakeTokenTest is Test {
    FakeToken fakeToken;
    address wallet1 = address(1);
    uint testAmount = 1 ether;

    function setUp() public {
        fakeToken = new FakeToken("DAI", "DAI");
    }

    function test_mint() public {
        fakeToken.mint(wallet1, testAmount);
        assertEq(fakeToken.balanceOf(wallet1), testAmount);
    }

    function test_burn() public {
        fakeToken.mint(wallet1, testAmount);
        vm.prank(wallet1);
        fakeToken.burn(testAmount);
        assertEq(fakeToken.balanceOf(wallet1), 0);
    }

    function test_setDecimals() public {
        assertEq(fakeToken.decimals(), 18);
        fakeToken.setDecimals(6);
        assertEq(fakeToken.decimals(), 6);
    }
}
