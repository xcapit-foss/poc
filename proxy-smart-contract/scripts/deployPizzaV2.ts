import { ethers, upgrades } from "hardhat";


async function main () {
    const PizzaV2 = await ethers.getContractFactory('PizzaV2');
    console.log('Deploying PizzaV2...');
    const pizzaV2 = await upgrades.deployProxy(PizzaV2, [8], { initializer: 'initialize' });
    await pizzaV2.deployed();
    console.log('PizzaV2 deployed to:', pizzaV2.address);
}

main();
