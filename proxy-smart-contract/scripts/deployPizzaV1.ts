import { ethers, upgrades } from "hardhat";


async function main () {
    const Pizza = await ethers.getContractFactory('Pizza');
    console.log('Deploying Pizza...');
    const pizza = await upgrades.deployProxy(Pizza, [7], { initializer: 'initialize' });
    await pizza.deployed();
    console.log('Pizza deployed to:', pizza.address);
}

main();
