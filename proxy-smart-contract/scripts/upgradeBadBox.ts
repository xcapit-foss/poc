import { ethers, upgrades } from "hardhat";


async function main () {
    const BadBox = await ethers.getContractFactory('BadBox');
    console.log('Upgrading BadBox...');
    await upgrades.upgradeProxy('0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0', BadBox);
    console.log('BadBox upgraded');
}

main();
