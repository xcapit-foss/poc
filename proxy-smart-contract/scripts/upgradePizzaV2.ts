import { ethers, upgrades } from "hardhat";

const PROXY = "0xC29C47bca2CB5fF7525bBd806FEbC0873EF16B08"

async function main() {
  const PizzaV2 = await ethers.getContractFactory("PizzaV2");
  console.log("Upgrading Pizza...");
  await upgrades.upgradeProxy(PROXY, PizzaV2);
  console.log("Pizza upgraded successfully");
}

main();
