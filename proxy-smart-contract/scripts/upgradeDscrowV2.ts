import { ethers, upgrades } from "hardhat";


const PROXY = "0x9fe46736679d2d9a65f0992f2272de9f3c7fa6e0";

async function main() {
    const DscrowV2 = await ethers.getContractFactory("DscrowV2");
    console.log("Upgrading DscrowV2...");
    await upgrades.upgradeProxy(PROXY, DscrowV2);
    console.log("DscrowV2 upgraded successfully");
}

main();
