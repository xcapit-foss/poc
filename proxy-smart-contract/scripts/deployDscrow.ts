import { ethers, upgrades } from "hardhat";


async function main () {
    const Doracle = await ethers.getContractFactory('Doracle');
    console.log('Deploying Doracle...');
    const doracle = await Doracle.deploy();
    console.log('Doracle deployed to:', doracle.address);
    const Dscrow = await ethers.getContractFactory('Dscrow');
    console.log('Deploying Dscrow...');
    const dscrow = await upgrades.deployProxy(Dscrow, [doracle.address], { initializer: 'initialize' });
    await dscrow.deployed();
    console.log('Dscrow deployed to:', dscrow.address);
    await doracle.setDscrow(dscrow.address);
    console.log('All set!');
}

main();
