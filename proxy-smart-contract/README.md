# Proxy Patterns

Una forma de hacer **upgradeables** los smart contracts deployados en la blockchain.

## Idea General

La idea es que la lógica de un smart contract sea **upgradeable** pero se pueda mantener el **estado/storage**. Para esto un smart contract **Proxy** se usa para mantener el **storage** y reenviar las llamadas, y un smart contract de **Implementación** es el que va a ir cambiando con las actualizaciones.
Algo importante para mencionar es que el **storage** se define en el smart contract de **Implementación** pero 'vive' en el smart contract **Proxy**. Esto hace que en una nueva versión del smart contract de **Implementación** podamos agregar nuevas variables, pero no modificar las ya existentes. Esto último se conoce como [Storage Collisions](https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies#storage-collisions-between-implementation-versions), y se tiene que [tener muy en cuenta](#cosas-a-tener-en-cuenta).

## Tipos de Proxy Pattern
Existen distintas implementaciones de Proxy Pattern, sólo vamos a nombrar y ver las más conocidas y las que, a nuestro criterio, se adaptan mejor a la solución que buscamos.

### UUPS
El UUPS consta de 2 smart contracts, un proxy y una implementación. La lógica para upgradear reside en el smart contract de implementación. Esto nos da varias ventajas
* Es más eficiente (GAS) al momento de deployar.
* No necesita mecanismos extras para evitar [Function Clashes](https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies#transparent-proxies-and-function-clashes).

EIP-1967: [Proxy Storage Slots](https://eips.ethereum.org/EIPS/eip-1967)
EIP-1882: [Universal Upgradeable Proxy Pattern (UUPS)](https://eips.ethereum.org/EIPS/eip-1822)

OpenZeppelin ofrece contratos y un plugin para implementar este pattern.

### Transparent
El Transparent Proxy Pattern consta de 3 smart contracts, un proxy, un proxy admin y una implementación. La razón de porque este pattern necesita un proxy admin es para evitar [Function Clashes](https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies#transparent-proxies-and-function-clashes).

EIP-1967: [Proxy Storage Slots](https://eips.ethereum.org/EIPS/eip-1967)

OpenZeppelin ofrece contratos y un plugin para implementar este pattern.

### Otros
Hay otros proxy patterns como el [Minimal Proxy Contract](https://eips.ethereum.org/EIPS/eip-1167) o el [Diammonds, Multi-Facet Proxy](https://eips.ethereum.org/EIPS/eip-2535) que me parecieron que no aplicaban o que eran demasiado complicados de utilizar.

### Conclusiones
Teniendo en cuenta de que UUPS es más eficiente (GAS) y comprende un smart contract menos, creo que es una buena opción para utilizar. [Transparent vs UUPS](https://docs.openzeppelin.com/contracts/4.x/api/proxy#transparent-vs-uups).

## OpenZeppelin Upgrades Plugins
OpenZeppelin ofrece varias herramientas para facilitarnos la implementación de un proxy. [Varios smart contract](https://docs.openzeppelin.com/contracts/4.x/api/proxy#transparent-vs-uups) para usar de base, como así también, un [plugin](https://docs.openzeppelin.com/learn/upgrading-smart-contracts) que se puede usar con Truffle o Hardhat. Con estas herramientas podemos implementar un Transparent Proxy como un UUPS.

### Transparent Example ([Box.sol](contracts/Box.sol))
Implementar el Transparent Proxy usando las herramientas de OpenZeppelin y Hardhat es bastante directo. En el repo, usamos el contrato [Box.sol](contracts/Box.sol) para deployarlo de manera upgradeable mediante un transparent proxy. Luego, actualizamos la implementación a [BoxV2.sol](contracts/BoxV2.sol). En el código de estos contratos no hay nada que indique que son upgradeables, eso es porque la lógica de hacer el upgrade está en el contrato proxy admin. Toda la magia está en los scripts de deploy y upgrade, ahí es donde usamos el plugin de OpenZeppelin para deployar [Box.sol](contracts/Box.sol), el plugin por detrás realiza un par de controles y deploya y configura los demas smart contracts (Proxy y Proxy Admin) necesarios para que [Box.sol](contracts/Box.sol) sea upgradeable. Nosotros no vemos los contratos Proxy y Proxy Admin, y cuando termina el deploy, la address que nos retorna es la address del Proxy, que es con quien tenemos que interactuar. Esta address es muy importante, ya que es la cual se va a mantener a medida que vayamos actualizando nuestro contrato de implementación.

#### [Script de deploy](scripts/deployUpgradeableBox.ts) de [Box.sol](contracts/Box.sol)
```typescript
import { ethers, upgrades } from "hardhat";


async function main () {
    const Box = await ethers.getContractFactory('Box');
    console.log('Deploying Box...');
    const box = await upgrades.deployProxy(Box, [42], { initializer: 'store' });
    await box.deployed();
    console.log('Box deployed to:', box.address);
}

main();
```

Como vemos, usando **upgrades** podemos deployar Box de manera upgradeable. El method **deployProxy** recibe la factory del contrato, un array de parámetros, y un **DeployProxyOptions** que en este caso le estamos pasando el method del contrato que es el initializer. El method initializer, cumple la función del constructor, y esto es necesario porque cuando aplicamos el proxy pattern no podemos usar constructores de la misma forma en que lo haciamos regularmente ([The Constructor Caveat](https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies#the-constructor-caveat)).

#### [Script de upgrade](scripts/upgradeBox.ts) a [BoxV2.sol](contracts/BoxV2.sol)
```typescript
import { ethers, upgrades } from "hardhat";


async function main () {
    const BoxV2 = await ethers.getContractFactory('BoxV2');
    console.log('Upgrading Box...');
    await upgrades.upgradeProxy('0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0', BoxV2);
    console.log('Box upgraded');
}

main();
```

Seguimos usando **upgrades**, pero en este caso el method **upgradeProxy**, le pasamos la address que retorno el script de deploy, y la factory del contrato BoxV2 que es una nueva versión de Box. El resultado de este upgrade es que seguimos manteniendo los mismos contratos de Proxy y Admin Proxy y actualizamos el de implementación (Box a BoxV2). La address con la que interactuamos sigue siendo la misma que nos retorno el script de deploy.

### UUPS Example ([Pizza.sol](contracts/Pizza.sol))
Implementar UUPS usando las herramientas de OpenZeppelin y Hardhat también es bastante directo. En el repo, usamos el contrato [Pizza.sol](contracts/Pizza.sol) para deployarlo de manera upgradeable mediante un UUPS. Luego, actualizamos la implementación a [PizzaV2.sol](contracts/PizzaV2.sol). En el código de estos contratos si necesitamos agregar la lógica relacionada al upgrade, ya que en UUPS no tenemos contrato Proxy Admin.

```solidity
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

contract Pizza is Initializable, UUPSUpgradeable, OwnableUpgradeable {

    uint256 slices;

    function initialize(uint256 sliceCount) public initializer {
        slices = sliceCount;
        __Ownable_init();
    }

    function _authorizeUpgrade(address) internal override onlyOwner {}

// .....
}
```

Como se puede ver, necesitamos heredar de **Initializable** y **UUPSUpgradeable**, como así también definir los methods **initialize** y **_authorizeUpgrade**. El method **initialize** se debe usar para inicializar el contrato, y el **_authorizeUpgrade** se debe reescribir para aplicarle algún control de acceso, en este caso, le agregamos el modifier **onlyOwner**. En este ejemplo también usamos el contrato **OwnableUpgradeable**, que es el mismo que el **Ownable**, pero teniendo en cuenta que va a ser usado en un contrato upgradeable.

Salvo las modificaciones al contrato que mencionamos, toda la magia sigue estando en los scripts de deploy y upgrade, en donde usamos el plugin de OpenZeppelin para deployar [Pizza.sol](contracts/Pizza.sol), el plugin por detras realiza un par de controles y deploya y configura los demás smart contracts (Proxy) necesarios para que [Pizza.sol](contracts/Pizza.sol) sea upgradeable. Nosotros no vemos el contrato Proxy, y cuando termina el deploy, la address que nos retorna es la address del Proxy, que es con quien tenemos que interactuar. Esta address es muy importante, ya que es la cual se va a mantener a medida que vayamos actualizando nuestro contrato de implementación.

#### [Script de deploy](scripts/deployPizzaV1.ts) de [Pizza.sol](contracts/Pizza.sol)
```typescript
import { ethers, upgrades } from "hardhat";


async function main () {
    const Pizza = await ethers.getContractFactory('Pizza');
    console.log('Deploying Pizza...');
    const pizza = await upgrades.deployProxy(Pizza, [8], { initializer: 'initialize' });
    await pizza.deployed();
    console.log('Pizza deployed to:', pizza.address);
}

main();
```

El script es idéntico al que utilizamos para deployar [Box.sol](contracts/Box.sol), pero el plugin de OpenZeppelin se da cuenta de que el contrato [Pizza.sol](contracts/Pizza.sol) hereda **UUPSUpgradeable** y entonces sabe que tiene que deployar un UUPS y no un Transparent Proxy.

#### [Script de upgrade](scripts/upgradePizzaV2.ts) a [PizzaV2.sol](contracts/PizzaV2.sol)
```typescript
import { ethers, upgrades } from "hardhat";

const PROXY = "0xC29C47bca2CB5fF7525bBd806FEbC0873EF16B08"

async function main() {
  const PizzaV2 = await ethers.getContractFactory("PizzaV2");
  console.log("Upgrading Pizza...");
  await upgrades.upgradeProxy(PROXY, PizzaV2);
  console.log("Pizza upgraded successfully");
}

main();
```

De nuevo, el script es idéntico al que usamos para upgradear [Box.sol](contracts/Box.sol) a [BoxV2.sol](contracts/BoxV2.sol). Pero como los contratos heredan de **UPPSUpgrade**, el plugin se da cuenta y actúa en consecuencia.

### UUPS Example ([Dscrow.sol](contracts/Dscrow.sol))
Con las pruebas ya realizadas y lo aprendido, nos surgió la duda de cómo utilizamos UUPS con nuestra estructura de Xscrow <-> Oracle, ya que los contratos están relacionados entre ellos. Por eso realizamos un mini ejemplo con el contrato [Dscrow.sol](contracts/Dscrow.sol) y [Doracle.sol](contracts/Doracle.sol) (la D es por Dummy🙃). El contrato upgradeable va a ser [Dscrow.sol](contracts/Dscrow.sol) y lo vamos a upgradear a [DscrowV2.sol](contracts/DscrowV2.sol).

El código del contrato que implementa la lógica de upgrade es igual al que usamos en [Pizza.sol](contracts/Pizza.sol), pero el script de deploy cambió un poco.

#### [Script de deploy](scripts/deployDscrow.ts) de ([Dscrow.sol](contracts/Dscrow.sol) + [Doracle.sol](contracts/Doracle.sol))
```typescript
import { ethers, upgrades } from "hardhat";


async function main () {
    const Doracle = await ethers.getContractFactory('Doracle');
    console.log('Deploying Doracle...');
    const doracle = await Doracle.deploy();
    console.log('Doracle deployed to:', doracle.address);
    const Dscrow = await ethers.getContractFactory('Dscrow');
    console.log('Deploying Dscrow...');
    const dscrow = await upgrades.deployProxy(Dscrow, [doracle.address], { initializer: 'initialize' });
    await dscrow.deployed();
    console.log('Dscrow deployed to:', dscrow.address);
    await doracle.setDscrow(dscrow.address);
    console.log('All set!');
}

main();
```

En este caso el deploy tiene un poco más de complejidad, pero no tanta. Empezamos deployando [Doracle.sol](contracts/Doracle.sol), luego deployamos ([Dscrow.sol](contracts/Dscrow.sol) y lo inicializamos pasandole la address de [Doracle.sol](contracts/Doracle.sol), al final le seteamos el la address de ([Dscrow.sol](contracts/Dscrow.sol) al ([Dscrow.sol](contracts/Dscrow.sol). Con eso ya quedan los contratos deployados y configurados. Quedan en la blockchain 3 contratos después de ejecutar este script (Proxy, Dscrow y Doracle). El contrato upgradeable es ([Dscrow.sol](contracts/Dscrow.sol).

#### [Script de upgrade](scripts/upgradeDscrowV2.ts) a [DscrowV2.sol](contracts/DscrowV2.sol)
```typescript
import { ethers, upgrades } from "hardhat";


const PROXY = "0x9fe46736679d2d9a65f0992f2272de9f3c7fa6e0";

async function main() {
    const DscrowV2 = await ethers.getContractFactory("DscrowV2");
    console.log("Upgrading DscrowV2...");
    await upgrades.upgradeProxy(PROXY, DscrowV2);
    console.log("DscrowV2 upgraded successfully");
}

main();
```

El script de upgrade es igual a los que vimos anteriormente, y acá se puede ver la ventaja del proxy, ya que [Doracle.sol](contracts/Doracle.sol) queda configurado con la address del Proxy, por lo tanto al upgradear la implementación de ([Dscrow.sol](contracts/Dscrow.sol) por [DscrowV2.sol](contracts/DscrowV2.sol) no tenemos que realizar ninguna configuración posterior.

### Conclusiones
Usando el plugin de OpenZeppelin y entendiendo lo que hace "por atrás", es bastante fácil utilizar un proxy para hacer que un contrato sea upgradeable. Creo que el UUPS es buen candidato para utilizar en nuestro caso de uso (Escrow + Oracle).


## Cosas a tener en cuenta
### No constructors (The Constructor Caveat)
[OpenZeppelin Docs Says](https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies#the-constructor-caveat).. In Solidity, code that is inside a constructor or part of a global variable declaration is not part of a deployed contract’s runtime bytecode. This code is executed only once, when the contract instance is deployed. As a consequence of this, the code within a logic contract’s constructor will never be executed in the context of the proxy’s state. To rephrase, proxies are completely oblivious to the existence of constructors. It’s simply as if they weren’t there for the proxy.

### Inicializar
Por lo expuesto con respecto al constructor, es muy recomendable usar **Initializable.sol** de OpenZeppelin y definir el method **initialize** para inicializar lo que comúnmente inicializamos en el constructor de un contrato.

### Add new variables al final
Ejemplo
```solidity
// contract v1
uint256 var1;
uint256 var2;
```

```solidity
// contract v2
uint256 var1;
uint256 var2;
uint256 var3; // new variable
```
Si hubiésemos agregado **var3** antes de **var1** estarimos generando una Esto es para evitar una [Storage Collisions](https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies#storage-collisions-between-implementation-versions) y que al escribir en **var3** escribimos en **var1**. En la doc de OpenZeppelin menciona que el plugin realiza controles en los contratos para detectar estos casos. Se comprobó deployando [Box.sol](contracts/Box.sol) e intentando upgradear a [BadBox.sol](contracts/BadBox.sol) (que tiene una storage collision) con [upgradeBadBox.ts](scripts/upgradeBadBox.ts).

### Considerar usar un contrato de base
Esta recomendación se encuentra en el [EIP-1822](https://eips.ethereum.org/EIPS/eip-1822#pitfalls-when-using-a-proxy), y hace referencia a desarrollar un contrato base con todas las variables y que un contrato con la lógica herede del base. Esto con el objetivo de minimizar la posibilidad de storage collisions.

Creo que esto es para evaluar al momento de implementarlo. Me parecio interesante el tema del gap en los contratos base que se trata en la [doc de OpenZeppelin](https://docs.openzeppelin.com/upgrades-plugins/1.x/writing-upgradeable#modifying-your-contracts).

### Restringir las funciones peligrosas
Esto es algo que no solo aplica cuando hablamos de proxies, pero el [EIP-1822](https://eips.ethereum.org/EIPS/eip-1822#restricting-dangerous-functions) también lo comenta de manera explícita.

### Múltiples deploys de un UUPS
Cuando usamos el plugin de OpenZeppelin y realizamos varios deploys de un contrato upgradeable por UUPS, lo que se deploya son instancias del proxy. O sea, un solo contrato es deployado y utiliza la implementación deployada anteriormente. Esto hace que ante la necesidad de deployar varios contratos el costo sea mucho menor, ya que solo se deploya el Proxy.

Por otro lado, es importante tener en cuenta de que al momento de upgradear la implementación, este upgrade se debe hacer de manera individual apuntando a todos los proxies deployados. Es por esto, que pienso se necesitaría llevar un registro de los proxies deployados para poder upgradear su contrato de implementación.

### Testing
En cuanto al testing de los contratos que implementen alguno de los patrones proxy, [OpenZeppelin recomienda](https://docs.openzeppelin.com/learn/upgrading-smart-contracts#testing) testear la implementación del contrato junto con un conjunto de test de más alto nivel que prueben la interacción con el proxy. En el caso de upgrade, es parecido, testear la implementación del nuevo contrato, la interacción con el proxy luego de realizar el upgrade, comprobando los estados del storage.

Pienso que puede ser válido agregar algunos test de alto nivel en el caso de que, después de un upgrade, queramos deployar otro contrato más de esa nueva versión.

Algo para pensar, es que este tipo de test (impl + proxy), me llevó a repetir mucho código de testing. No está en el scope de este spike resolver esto, pero creo válido mencionarlo para tenerlo en cuenta.
### Deploy & Upgrade
Vimos que con un script de upgrade podemos upgradear de Box a BoxV2, pero si queremos deployar otro contrato BoxV2 debemos realizar un nuevo script de deploy. Por lo que para el ejemplo nos quedarían 3 scripts (deployBox.ts, upgradeBoxV2.ts, deployBoxV2.ts). Y lo mismo aplica para futuras versiones de Box.
### Recomendaciones de OpenZeppelin
Muchas de las recomendaciones de OpenZeppelin fueron ya mencionadas, de todas maneras, la lista completa se encuentra en el siguiente [link](https://docs.openzeppelin.com/upgrades-plugins/1.x/writing-upgradeable).


## Yapa

### Deploy en Tesnet + Verify
En el file [hardhat.config.ts](hardhat.config.ts), esta configurada la testnet Sepolia. Lo que tenemos que hacer ahora es completar el file de variables de entorno.
En consola
```bash
cp .env.example .env
```

y después editamos **.env**
```bash
PRIVATE_KEY= # wallet private key
ETHERSCAN_API_KEY= # etherscan api key, se pueden crear una cuenta en https://etherscan.io/ y desde ahi generar una api key
RPC_URL= # pueden usar cualqueir RPC de Sepolia, ej.: https://ethereum-sepolia.blockpi.network/v1/rpc/public
```
Ahora deployamos, por ejemplo, [Pizza.sol](contracts/Pizza.sol) con el siguiente comando
```bash
npx hardhat run scripts/deployPizzaV1.ts --network sepolia
```
Esto termina con el contrato deployado en Sepolia, y ahora para verificar el contrato de implementación tenemos que ejecutar
```bash
npx hardhat verify --network sepolia <<IMPLEMENTATION_CONTRACT_ADDRESS>>
```
La IMPLEMENTATION\_CONTRACT\_ADDRESS la pueden obtener de las txs en el [scan de Sepolia](https://sepolia.etherscan.io/).

Buscamos el contrato Proxy en el scan, entramos en la tab **Contract** y buscamos la opción **More Options** y seleccionamos **Is this a Proxy?**, seguimos los pasos y listo, ahora podemos interactuar con [Pizza.sol](contracts/Pizza.sol) desde el scan y mediante el Proxy.

Todos estos pasos con capturas de pantalla y un poco mas detallados se pueden encontrar en [este post](https://blog.logrocket.com/using-uups-proxy-pattern-upgrade-smart-contracts/).

## Links
* [OpenZeppelin, Contratos, Doc y UUPS vs Transparent](https://docs.openzeppelin.com/contracts/4.x/api/proxy)
* [OpenZeppelin, Proxy Pattern un poco mas deep](https://blog.openzeppelin.com/proxy-patterns/)
* [OpenZeppelin, Doc](https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies)
* [OpenZeppelin, Upgrades Plugins Overview](https://docs.openzeppelin.com/upgrades-plugins/1.x/)
* [OpenZeppelin, Upgrading Smart Contracts using the OZ plugin](https://docs.openzeppelin.com/learn/upgrading-smart-contracts)
* [OpenZeppelin, Contratos para usar con Upgrades](https://docs.openzeppelin.com/contracts/4.x/upgradeable)
* [OpenZeppelin, On writing Upgradeable Contracts](https://docs.openzeppelin.com/upgrades-plugins/1.x/writing-upgradeable)
* [OpenZeppelin, Recomendaciones con respecto al Testing](https://docs.openzeppelin.com/learn/upgrading-smart-contracts#testing)
* [EIP-1822](https://eips.ethereum.org/EIPS/eip-1822)
* [EIP-1967](https://eips.ethereum.org/EIPS/eip-1967)
* [Post bastante completo sobre UUPS](https://blog.logrocket.com/using-uups-proxy-pattern-upgrade-smart-contracts/)
* [Ejemplo deployado en Sepolia](https://sepolia.etherscan.io/address/0xC29C47bca2CB5fF7525bBd806FEbC0873EF16B08#code)
* [Post que incluye tests](https://dev.to/yakult/tutorial-write-upgradeable-smart-contract-proxy-contract-with-openzeppelin-1916) (cuestionable como organiza los tests, pero creo que puede servir de ejemplo)
