import { loadFixture } from '@nomicfoundation/hardhat-network-helpers';
import { expect } from 'chai';
import { ethers } from 'hardhat';
import { Pizza } from '../typechain-types';

describe('Pizza Impl', function () {
  const aNumberOfSlices = 8;

  async function deployPizza() {
    const [owner, anotherAccount] = await ethers.getSigners();
    const Pizza = await ethers.getContractFactory('Pizza');
    const pizza: Pizza = await Pizza.deploy();

    return { pizza, owner, anotherAccount };
  }

  it('Deployed whit default values', async function () {
    const { pizza } = await loadFixture(deployPizza);

    expect(await pizza.slices()).to.equal(0);
    expect(await pizza.owner()).to.equal(ethers.constants.AddressZero);
  });

  it('Initialize', async function () {
    const { pizza, owner } = await loadFixture(deployPizza);

    await pizza.initialize(aNumberOfSlices);

    expect(await pizza.owner()).to.equal(owner.address);
    expect(await pizza.slices()).to.equal(aNumberOfSlices);
  });

  it('Initialize twice', async function () {
    const { pizza } = await loadFixture(deployPizza);

    await pizza.initialize(aNumberOfSlices);

    await expect(pizza.initialize(aNumberOfSlices)).to.revertedWith('Initializable: contract is already initialized');
  });

  it('eat slice', async function () {
    const { pizza } = await loadFixture(deployPizza);
    await pizza.initialize(aNumberOfSlices);

    await pizza.eatSlice();

    expect(await pizza.slices()).to.equal(aNumberOfSlices - 1);
  });
});
