import { loadFixture } from '@nomicfoundation/hardhat-network-helpers';
import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Pizza } from '../typechain-types';

describe('Pizza Proxy', function () {
  const aNumberOfSlices = 8;

  async function deployPizza() {
    const [owner, wallet1, wallet2] = await ethers.getSigners();
    const Pizza = await ethers.getContractFactory('Pizza');
    const pizza = await upgrades.deployProxy(Pizza, [aNumberOfSlices], { initializer: 'initialize' });

    return { pizza, owner, wallet1, wallet2 };
  }

  it('Deployed whit default values', async function () {
    const { pizza, owner } = await loadFixture(deployPizza);
    expect(await pizza.owner()).to.equal(owner.address);
    expect(await pizza.slices()).to.equal(aNumberOfSlices);
  });

  it('eat slice', async function () {
    const { pizza } = await loadFixture(deployPizza);

    await pizza.eatSlice();

    expect(await pizza.slices()).to.equal(aNumberOfSlices - 1);
  });

  it('only owner can upgrade', async function () {
    const { pizza, wallet1, wallet2 } = await loadFixture(deployPizza);

    await expect((pizza as Pizza).connect(wallet1).upgradeTo(wallet2.address)).to.revertedWith(
      'Ownable: caller is not the owner'
    );
  });
});
