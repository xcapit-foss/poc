// contracts/Dscrow.sol
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./Doracle.sol";

contract Dscrow is Initializable, UUPSUpgradeable, OwnableUpgradeable {

    address _oracle;
    address[] _calles;

    function initialize(address anOracle) public initializer {
        _oracle = anOracle;
        __Ownable_init();
    }

    function _authorizeUpgrade(address) internal override onlyOwner {}

    function callOracle() external {
        _calles.push(msg.sender);
        Doracle(_oracle).test();
    }

    function fromOracle() external {
        _calles.push(msg.sender);
    }

    function calles() external view returns (address[] memory) {
        return _calles;
    }

    function oracle() external view returns (address) {
        return _oracle;
    }
}
