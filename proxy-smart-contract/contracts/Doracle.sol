// contracts/Doracle.sol
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import "./Dscrow.sol";

contract Doracle {

    address _dscrow;
    address[] _calles;

    function test() external {
        require(msg.sender == _dscrow, "You are not the Dscrow...");
        _calles.push(msg.sender);
    }

    function setDscrow(address aDscrow) external {
        _dscrow = aDscrow;
    }

    function callDscrow() external {
        _calles.push(msg.sender);
        Dscrow(_dscrow).fromOracle();
    }

    function calles() external view returns (address[] memory) {
        return _calles;
    }

    function dscrow() external view returns (address) {
        return _dscrow;
    }
}
