// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

interface IFakeXscrow {
    function withdrawOf(address payee) external;
}

contract FakeXscrow is IFakeXscrow {
    // struct {balance, appval, timestamp}
    mapping(address => bool) private _approvals;
    mapping(address => uint256) public _deposits;

    function withdrawOf(address payee) public {
        _approvals[payee] = true;
    }
}
