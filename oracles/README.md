# Oracles

Una muy buena explicación para entender que son y porque se necesitan los oráculos se puede leer en este [artículo de chainlink](https://chain.link/education/blockchain-oracles), pero para resumir, un oráculo nos permite consultar información que esta fuera de la blockchain (off-chain) desde la blockchain (on-chain).

> Chainlink: decentralized oracle networks provide tamper-proof inputs, outputs, and computations to support advanced smart contracts on any blockchain.

## Caso de Uso

En nuestro caso, tenemos un contrato en la blockchain que necesita solicitar data off-chain para poder decidir si realizar o no una determinada acción.

Al principio nos imaginamos algo como esto
```
// pseudocode
if (oracle.offChainData()) {
.
.
}
```
La realidad y la forma de implementación de las herramientas que nos provee chainlink es otra.
El flujo que nos da chainlink para armar un oráculo es el siguiente
```
contract request ---> oracle implementation ----> chainlink oracles network (fin)
and
chainlink oracles network (obtuvo el dato solicitado) -> oracle implementation (fin)
```
La mayoría de las implementaciones de ejemplo y la propia [documentación de chainlink](https://docs.chain.link/any-api/get-request/examples/single-word-response) muestran esa dinámica.
Desechamos nuestra solución usando un ``if`` silvestre.

Nos dimos cuenta de que en el segundo paso, podíamos ejecutar un poco más de código y planteamos lo siguiente
```
contract request ---> oracle implementation ----> chainlink oracles network (fin)
and
chainlink oracles network (obtuvo el dato solicitado) -> oracle implementation -> contract (fin)
```

Partiendo del ejemplo de la [documentación de chainlink](https://docs.chain.link/any-api/get-request/examples/single-word-response) llegamos a esta prueba de concepto

```solidity
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "./FakeXscrow.sol";
import "@chainlink/contracts/src/v0.8/ChainlinkClient.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract CreditOracle is ChainlinkClient, Ownable {
    using Chainlink for Chainlink.Request;

    bytes32 private jobId;
    uint256 private fee;

    /* event RequestWithdraw(bytes32 indexed requestId, address _address); */

    /**
     * @notice Initialize the link token and target oracle
     *
     * Goerli Testnet details:
     * Link Token: 0x326C977E6efc84E512bB9C30f76E30c160eD06FB
     * Oracle: 0xCC79157eb46F5624204f47AB42b3906cAA40eaB7 (Chainlink DevRel)
     * jobId: ca98366cc7314957b8c012c72f05aeeb
     *
     */
    constructor() {
        setChainlinkToken(0x326C977E6efc84E512bB9C30f76E30c160eD06FB);
        setChainlinkOracle(0xCC79157eb46F5624204f47AB42b3906cAA40eaB7);
        jobId = "7da2702f37fd48e5b1b9a5715e3509b6";
        fee = (1 * LINK_DIVISIBILITY) / 10; // 0,1 * 10**18 (Varies by network and job)
    }

    /**
     * Create a Chainlink request to retrieve API response, find the target
     * data, then multiply by 1000000000000000000 (to remove decimal places from data).
     */
    function requestCreditData(
        address _address
    ) public returns (bytes32 requestId) {
        Chainlink.Request memory req = buildChainlinkRequest(
            jobId,
            address(this),
            this.fulfill.selector
        );

        req.add("get", _url(_address));
        req.add("path", "address");

        // Multiply the result by 1000000000000000000 to remove decimals
        int256 timesAmount = 10 ** 18;
        req.addInt("times", timesAmount);

        return sendChainlinkRequest(req, fee);
    }

    function _url(address address_) public pure returns (string memory) {
        return
            string(
                abi.encodePacked(
                    "https://xcapit.com/?payee=",
                    Strings.toHexString(uint256(uint160(address_)), 20)
                )
            );
    }

    function fulfill(
        bytes32 _requestId,
        bytes calldata address_
    ) public recordChainlinkFulfillment(_requestId) {
        IFakeXscrow(0x854F450780Ba2800765b977378EFC1065CA46a35).withdrawOf(
            _bytesToAddress(address_)
        );
    }

    function _bytesToAddress(
        bytes memory bys
    ) private pure returns (address addr) {
        assembly {
            addr := mload(add(bys, 20))
        }
    }

    function withdrawLink() public onlyOwner {
        LinkTokenInterface link = LinkTokenInterface(chainlinkTokenAddress());
        require(
            link.transfer(msg.sender, link.balanceOf(address(this))),
            "Unable to transfer"
        );
    }
}
```
Paso a explicar las cosas que nos parecen mas importantes y que están explícitamente relacionadas con chainlink oracles.

**setChainlinkToken(0x326C977E6efc84E512bB9C30f76E30c160eD06FB);**
El contrato debe tener configurada la address del token LINK. Esto se debe a que el uso de la chainlink oracles network no es gratis. Cada consulta cuesta LINKs, y esos LINKs se cobran de este contrato. Con esto también estamos diciendo que el contrato tiene que tener balance en LINK. [LINK Token Address by Blockchain](https://docs.chain.link/resources/link-token-contracts/)

**setChainlinkOracle(0xCC79157eb46F5624204f47AB42b3906cAA40eaB7);**
Cada blockchain tiene su chainlink oracle address, y la misma la tenemos que setear. [Testnet Oracles](https://docs.chain.link/any-api/testnet-oracles/)

**jobId = "7da2702f37fd48e5b1b9a5715e3509b6";**
Dependiendo del tipo de dato off-chain que necesitamos traer, debemos configurar un jobId diferente. [Testnet JobIds](https://docs.chain.link/any-api/testnet-oracles/#job-ids)

**fee = (1 * LINK_DIVISIBILITY) / 10; // 0,1 * 10**18 (Varies by network and job)**
Lo que nos cobran chainlink en LINK por cada request a data off-chain. No encontramos mucha definición de que valor es el correcto para un caso productivo.

**function requestCreditData(address _address)..**
Método que usaríamos para requerir data off-chain.

**req.add("path", "address");**
Esto que esta dentro de `requestCreditData` puede ser un poco confuso, pero en resumen, en la misma request hay que indicarle donde encontrar el dato. Ej. el api que consultamos retorna 
```json
{
  "address": "...",
  "dato1": { "dato2": "..." }
}
```
y queremos acceder a `dato2`, entonces deberíamos indicarlo así
```solidity
req.add("path", "address,dato1,dato2");
``` 
para acceder a más de una dato hay que repetir la instrucción y utilizar un jobId que lo soporte.

**function fulfill(bytes32 _requestId, bytes calldata address_)..**
Método que usa un oráculo de chainlink para responder a la request de `requestCreditData`.

## Links
* [Repo en donde estamos desarrollando el escrow y el oráculo](https://gitlab.com/xcapit-foss/xscrow)
* [Ejemplo de chainlink que usamos para empezar](https://docs.chain.link/any-api/get-request/examples/single-word-response)
* [Info de chainlink de oráculos y jobsids para testnet](https://docs.chain.link/any-api/testnet-oracles/)
* [Address del token LINK para distintas blockchains](https://docs.chain.link/resources/link-token-contracts/)
* [Repo de los contratos de Chainlink](https://github.com/smartcontractkit/chainlink/tree/develop/contracts)
* [Tools de Chainlink en github](https://github.com/smartcontractkit)
* [Intro de chainlink sobre oráculos](https://chain.link/education/blockchain-oracles)
