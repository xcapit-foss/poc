import { confirmTx } from './confirm-tx';
import { executeTx } from './execute-tx';
import { sendETHToSafe } from './funding-safe';
import { deploySafe } from './new-safe-multisig';
import { nextNonce } from './next-nonce';
import { signers } from './owners';
import { safePendingTx } from './pending-txs';
import {
  proposeApproveERC20Tx,
  proposeDepositToXscrowTx,
} from './propose-contract-tx';
import { proposeSendTx } from './propose-send-tx';
import { safeSdkFrom } from './safe-sdk';

(async () => {
  // await deploySafe();
  // await sendETHToSafe();

  // await proposeSendTx();
  // await confirmTx();
  // await executeTx();

  // await proposeApproveERC20Tx();
  // await confirmTx();
  // await executeTx();

  // await proposeDepositToXscrowTx();
  // await confirmTx();
  // await executeTx();

  console.log('pending txs:', await safePendingTx(signers.owner1()));
  console.log('next nonce: ', await nextNonce());
})();
