import dotenv from 'dotenv';

dotenv.config();

export const {
  RPC_URL,
  OWNER1_PK,
  OWNER1_ADDRESS,
  OWNER2_PK,
  OWNER2_ADDRESS,
  OWNER3_PK,
  OWNER3_ADDRESS,
  TX_SERVICE_URL,
  SAFE_ADDRESS,
  XSCROW_ADDRESS,
  LINK_ADDRESS,
} = process.env;
