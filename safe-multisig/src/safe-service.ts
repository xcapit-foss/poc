import { ethers } from 'ethers';
import { TX_SERVICE_URL } from './constants';
import SafeApiKit from '@safe-global/api-kit';
import { EthersAdapter } from '@safe-global/protocol-kit';

export const safeServiceFrom = async (aSignerOwner: any) => {
  return new SafeApiKit({
    txServiceUrl: TX_SERVICE_URL!,
    ethAdapter: new EthersAdapter({
      ethers,
      signerOrProvider: aSignerOwner,
    }),
  });
};
