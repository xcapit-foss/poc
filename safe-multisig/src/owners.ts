import { ethers } from 'ethers';
import {
  OWNER1_PK,
  OWNER1_ADDRESS,
  OWNER2_PK,
  OWNER2_ADDRESS,
  OWNER3_PK,
  OWNER3_ADDRESS,
  RPC_URL,
} from './constants';

const provider = () => new ethers.providers.JsonRpcProvider(RPC_URL);

export const signers = {
  owner1: () => new ethers.Wallet(OWNER1_PK!, provider()),
  owner2: () => new ethers.Wallet(OWNER2_PK!, provider()),
  owner3: () => new ethers.Wallet(OWNER3_PK!, provider()),
};

export const addresses = {
  owner1: () => `0x${OWNER1_ADDRESS}`,
  owner2: () => `0x${OWNER2_ADDRESS}`,
  owner3: () => `0x${OWNER3_ADDRESS}`,
};
