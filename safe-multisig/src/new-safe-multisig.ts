import { ethers } from 'ethers';
import {
  EthersAdapter,
  SafeFactory,
  SafeAccountConfig,
} from '@safe-global/protocol-kit';
import { addresses, signers } from './owners';

export async function deploySafe() {
  // Initialize Signers, Providers, and EthAdapter
  const ethAdapterOwner1 = () =>
    new EthersAdapter({ ethers, signerOrProvider: signers.owner1() });

  // Initialize the Protocol Kit
  const safeFactory = async () =>
    await SafeFactory.create({ ethAdapter: ethAdapterOwner1() });

  // Deploy a Safe
  const safeAccountConfig = (): SafeAccountConfig => ({
    owners: [addresses.owner1(), addresses.owner2(), addresses.owner3()],
    threshold: 2,
  });

  console.log('Deploying Safe...');
  const safeSdkOwner1 = await (await safeFactory()).deploySafe({
    safeAccountConfig: safeAccountConfig(),
  });

  console.log('Your Safe has been deployed:');
  console.log(
    `https://goerli.etherscan.io/address/${await safeSdkOwner1.getAddress()}`,
  );
  console.log(
    `https://app.safe.global/gor:${await safeSdkOwner1.getAddress()}`,
  );

  // INFO: output
  // https://goerli.etherscan.io/address/0xAbBF5Ee17271B7E96310ECC9681aFFA5daE57aEA
  // https://app.safe.global/gor:0xAbBF5Ee17271B7E96310ECC9681aFFA5daE57aEA
}
