import { ethers } from 'ethers';
import { signers } from './owners';
import { safeSdkFrom } from './safe-sdk';

export async function sendETHToSafe() {
  // Send ETH to the Safe
  const tx = await signers.owner1().sendTransaction({
    to: await (await safeSdkFrom(signers.owner1())).getAddress(),
    value: ethers.utils.parseUnits('0.1', 'ether').toHexString(),
  });

  console.log('Fundraising.');
  console.log(`Deposit Transaction: https://goerli.etherscan.io/tx/${tx.hash}`);
}
