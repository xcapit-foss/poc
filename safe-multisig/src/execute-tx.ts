import { signers } from './owners';
import { safePendingTx } from './pending-txs';
import { safeSdkFrom } from './safe-sdk';
import { safeServiceFrom } from './safe-service';

export const executeTx = async () => {
  // Execute the transaction
  const safeSDK = await safeSdkFrom(signers.owner1());
  const safeService = await safeServiceFrom(signers.owner1());

  const tx = (await safePendingTx(signers.owner1())).pop();

  const safeTx = await safeService.getTransaction(tx!.safeTxHash);
  const executedTxResponse = await safeSDK.executeTransaction(safeTx);
  const receipt = await executedTxResponse.transactionResponse?.wait();

  console.log('Transaction executed:');
  console.log(`https://goerli.etherscan.io/tx/${receipt?.transactionHash}`);
};
