import { ethers } from 'ethers';
import { nextNonce } from './next-nonce';
import { signers } from './owners';
import { proposeTx } from './propose-tx';
import { safeSdkFrom } from './safe-sdk';

export const proposeSendTx = async () => {
  const safeSDK = await safeSdkFrom(signers.owner1());

  const safeTx = await safeSDK.createTransaction({
    safeTransactionData: {
      to: await signers.owner1().getAddress(),
      data: '0x',
      value: ethers.utils.parseUnits('0.001', 'ether').toString(),
      nonce: await nextNonce(),
    },
  });

  await proposeTx(signers.owner1(), safeTx);
};
