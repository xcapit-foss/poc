import { SafeTransaction } from '@safe-global/safe-core-sdk-types';
import { ethers } from 'ethers';
import { signers } from './owners';
import { safeSdkFrom } from './safe-sdk';
import { safeServiceFrom } from './safe-service';

export const proposeTx = async (
  anOwner: ethers.Wallet,
  aSafeTx: SafeTransaction,
) => {
  const safeSDK = await safeSdkFrom(signers.owner1());

  const safeTxHash = await safeSDK.getTransactionHash(aSafeTx);
  const senderSignature = await safeSDK.signTransactionHash(safeTxHash);

  await (await safeServiceFrom(signers.owner1())).proposeTransaction({
    safeAddress: await safeSDK.getAddress(),
    safeTransactionData: aSafeTx.data,
    safeTxHash,
    senderAddress: await anOwner.getAddress(),
    senderSignature: senderSignature.data,
  });
};
