import { signers } from './owners';
import { safePendingTx } from './pending-txs';
import { safeSdkFrom } from './safe-sdk';

export const nextNonce = async () => {
  return (
    (await (await safeSdkFrom(signers.owner1())).getNonce()) +
    (await safePendingTx(signers.owner1())).length
  );
};
