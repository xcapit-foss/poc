import { ethers } from 'ethers';
import { safeSdkFrom } from './safe-sdk';
import { safeServiceFrom } from './safe-service';

export const safePendingTx = async (anOwner: ethers.Wallet) => {
  return (
    await (await safeServiceFrom(anOwner)).getPendingTransactions(
      await (await safeSdkFrom(anOwner)).getAddress(),
    )
  ).results;
};
