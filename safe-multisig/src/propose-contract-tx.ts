import { ethers } from 'ethers';
import { LINK_ADDRESS, XSCROW_ADDRESS } from './constants';
import { nextNonce } from './next-nonce';
import { addresses, signers } from './owners';
import { proposeTx } from './propose-tx';
import { safeSdkFrom } from './safe-sdk';

export const proposeApproveERC20Tx = async () => {
  // Create a contract transaction
  const safeSDK = await safeSdkFrom(signers.owner1());

  const interfaceERC20 = new ethers.utils.Interface([
    'function approve(address _spender, uint256 _value)',
  ]);
  const encodedData = () =>
    interfaceERC20.encodeFunctionData('approve', [
      `0x${XSCROW_ADDRESS!}`,
      ethers.utils.parseUnits('1000', 'ether').toString(),
    ]);

  const safeTx = await safeSDK.createTransaction({
    safeTransactionData: {
      to: `0x${LINK_ADDRESS!}`,
      data: encodedData(),
      value: '0',
      nonce: await nextNonce(),
    },
  });

  // Propose the transaction
  await proposeTx(signers.owner1(), safeTx);
};

export const proposeDepositToXscrowTx = async () => {
  // Create a contract transaction
  const safeSDK = await safeSdkFrom(signers.owner1());
  const interfaceXscrow = new ethers.utils.Interface([
    'function depositTo(address aPayee, uint256 anAmount)',
  ]);
  const encodedData = () =>
    interfaceXscrow.encodeFunctionData('depositTo', [
      addresses.owner3(),
      ethers.utils.parseUnits('0.3', 'ether').toString(),
    ]);

  const safeTx = await safeSDK.createTransaction({
    safeTransactionData: {
      to: `0x${XSCROW_ADDRESS!}`,
      data: encodedData(),
      value: '0',
      nonce: await nextNonce(),
    },
  });

  // Propose the transaction
  await proposeTx(signers.owner1(), safeTx);
};
