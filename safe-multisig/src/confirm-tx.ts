import { signers } from './owners';
import { safePendingTx } from './pending-txs';
import { safeSdkFrom } from './safe-sdk';
import { safeServiceFrom } from './safe-service';

export const confirmTx = async () => {
  // Confirm the transaction: second confirmation
  const safeSDK = await safeSdkFrom(signers.owner2());
  const safeService = await safeServiceFrom(signers.owner2());

  const tx = (await safePendingTx(signers.owner2())).pop();

  const signature = await safeSDK.signTransactionHash(tx!.safeTxHash);
  const response = await safeService.confirmTransaction(
    tx!.safeTxHash,
    signature.data,
  );

  console.log('response', response);
};
