import { ethers } from 'ethers';
import Safe, { EthersAdapter } from '@safe-global/protocol-kit';
import { SAFE_ADDRESS } from './constants';

export const safeSdkFrom = async (aSignerOwner: any) => {
  return await Safe.create({
    safeAddress: `0x${SAFE_ADDRESS!}`,
    ethAdapter: new EthersAdapter({
      ethers,
      signerOrProvider: aSignerOwner,
    }),
  });
};
