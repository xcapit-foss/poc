# TODO

- [x] Deploy Safe
- [x] Send ETH to Safe
- [x] Propose a send Transaction
- [x] Propose a contract interaction Transaction
- [x] Confirm a Transaction
- [x] Execute a Transaction
- [x] Batch Transaction

# Safe Multisig
Para esta prueba de concepto se utilizaron las libs de Safe [Protocol Kit](https://docs.safe.global/safe-core-aa-sdk/protocol-kit) y [API Kit](https://docs.safe.global/safe-core-aa-sdk/api-kit). Usamos [Protocol Kit](https://docs.safe.global/safe-core-aa-sdk/protocol-kit) para comunicarnos con los contratos de Safe en la blockchain y [API Kit](https://docs.safe.global/safe-core-aa-sdk/api-kit) para comunicarnos con los servicios off-chain de Safe, como por ejemplo el servicio que mantiene las transacciones off-chain mientras las mismas son aprobadas por los distintos signers de la wallet Safe Multisig.

Para una introducción sobre qué es Safe Protocol se puede ver el sig. [link](https://docs.safe.global/getting-started/readme).

## Configuration
Es necesario crear el file con las variables de entorno, para eso podemos ejecutar
```bash
cp .env.example .env
```
Algunas de las variables ya están definidas con datos válidos para ejecutar los scripts en Goerli. Las variables a completar son las address y las private keys de 3 wallets que se usan para crear una multisig 2/3 y la address de la Safe Multisig creada, esta última variable la vamos a obtener después de ejecutar el [script de deploy](./src/new-safe-multisig.ts).

## Deploy Safe
En el [script de deploy](./src/new-safe-multisig.ts) lo que hacemos es deployar el contrato de la safe multisig en la blockchain Goerli. Esta safe multisig se deploya con la [configuración](./src/new-safe-multisig.ts#L20) de que va a tener 3 owners y que para poder ejecutar una transaction se va a requerir que 2 de los 3 owners aprueben esa transaction.

Terminamos con la safe mutlisig deployada en Goerli y mostrando por consola la address de la misma, esta address es la que tenemos que usar para completar la variable de entorno `SAFE_ADDRESS`.

## Sen ETH to Safe
Este [script](./src/funding-safe.ts) transfiere GoerliETH (token nativo en Goerli) a la safe multisig. Esto lo hacemos para poder hacer el script siguiente, que es una propose transaction de un send.

## Propose a Send Transaction
En [propose-send-tx](./src/propose-send-tx.ts) hacemos lo que el nombre indica, creamos una propose transaction y la enviamos al servicio de Safe. Particularmente esta transaction es un send de token nativo y no tiene nada muy especial. Cuando proponemos una transaction también la estamos firmando, por lo que la transaction propuesta ya va a tener la primera confirmación (1/3).

Un valor que puede llamar la atención en el script es el `nonce`, esto lo vamos a explicar [más adelante](#nonce).

## Confirm a Transaction
En [confirm-tx](./src/confirm-tx.ts) pasan dos cosas, por un lado obtenemos la lista de las pendings transactions y por otro usamos una owner diferente al que propuso la transaction para firmar y después enviar la confirmación de la transaction. En el ejemplo, el mismo owner que está confirmando la transaction la podría ejecutar, ya que con su firma la transaction tiene 2 confirmaciones y pasa a estar 2/3 que es la cantidad de firmas que necesita, pero para separar el código, la ejecución de la transaction pasa en otro script diferente.

## Execute a Transaction
[execute-tx](./src/execute-tx.ts) no hace más que obtener las pendings transactions, seleccionar una y ejecutarla. Para simplificar el código no realizamos ningún control sobre el número de confirmaciones que tiene la transaction a ejecutar. En este caso la wallet owner que ejecute la transaction va a pagar por el fee de la misma.

## Propose a Contract Interaction Transaction
Para ejemplificar esta acción realizamos dos transactions, un [approve de token ERC20](./src/propose-contract-tx.ts#L8) y un [depositTo de Xscrow](./src/propose-contract-tx.ts#L34).

[approve de token ERC20](./src/propose-contract-tx.ts#L8), le damos permiso al contrato de Xscrow para que pueda transferir una cantidad de token específica de la wallet safe multisig. La particularidad de realizar una transaction contra un contrato es que tenemos que encodear la data para poder armar dicha transaction, y cada tipo de transaction va a necesitar un encoded propio, esto se hace con [encodedData](./src/propose-contract-tx.ts#L15).

[depositTo de Xscrow](./src/propose-contract-tx.ts#L34), en este caso el código es muy similar al anterior, pero cambia la forma en cómo se realiza el encoded de la data para armar la transaction, esto se puede ver en [encodedData](./src/propose-contract-tx.ts#L40).

Tener en cuenta que estos son solo proposes, para ejecutarlas hay que seguir los pasos de confirmar y luego ejecutar, esto se hace con los scripts ya nombrados [confirm-tx](./src/confirm-tx.ts) y [execute-tx](./src/execute-tx.ts).

## Batch Transaction
Safe ofrece, mediante unos contratos propios, funcionalidades extras. Uno de estos contratos permite ejecutar varias transactions como si fuera una sola. Esto se puede hacer mediante código, pero también se puede hacer por medio de la interfaz gráfica de la wallet safe multisig.

Creo que si necesitamos ejecutar un batch de transactions, y las estas no son tantas, podemos usar una solución intermedia. Crear las propose transactions y confirmarlas por código y crear el batch para ejecutar desde la interfaz gráfica.

Info sobre hacerlo mediante código
* [Repo de Safe para interacción con los contratos](https://github.com/safe-global/safe-deployments/tree/main)
* [Post en donde se trata batch tx por código](https://ethereum.stackexchange.com/questions/151414/how-to-use-batch-transaction-feature-programatically)

## Notes

### Nonce
Safe multisig lleva cuenta del nonce de las transactions que ejecuta. Esto lo hace on-chain, esa cuenta está en el contrato deployado en la blockchain. Cuando obtenemos el nonce desde safe, se lee el contrato en la blockchain y se retorna el próximo número que debería usar la próxima transaction. El tema es que cuando se realizan las propose transactions todo ese proceso pasa off-chain, por lo que sí creo 3 propose transactions y dejo el nonce por defecto las 3 van a usar el mismo número de nonce y esto hace que cuando se ejecute una de esas 3 transactions, las 2 restantes (con el mismo nonce) se eliminen.

Planteado el problema, se implementó en [next-nonce](./src/next-nonce.ts) un cálculo del próximo nonce a usar mezclando data on-chain y off-chain. Por un lado pedimos el nonce al contrato en la blockchain y por otro pedimos la lista de pending transactions. Sumamos al nonce retornado de la blockchain la cantidad de transactions pendientes (length del array) y retornamos el nonce a usar.

### Pending Transactions
La obtención de las pending transactions está implementada en [pending-txs](./src/pending-txs.ts) y las retorna en un array. Es importante mencionar que si tenemos más de una pending transaction en el array, la que hay que ejecutar es la última y no la primera. Un ejemplo de esto último se puede ver en [execute-tx](./src/execute-tx.ts#L11) en que la transaction se obtiene del array utilizando un pop.

Ejemplo de pending transactions array
```json
[
  {
    //.
    //.
    nonce: 14
    //.
    //.
  },
  {
    //.
    //.
    nonce: 13
    //.
    //.
  },
]
```

## Links
* [Protocol Kit](https://docs.safe.global/safe-core-aa-sdk/protocol-kit)
* [API Kit](https://docs.safe.global/safe-core-aa-sdk/api-kit)
* [Safe getting started documentation](https://docs.safe.global/getting-started/readme)
* [Repo de Safe para interacción con los contratos](https://github.com/safe-global/safe-deployments/tree/main)
* [Post en donde se trata batch tx por código](https://ethereum.stackexchange.com/questions/151414/how-to-use-batch-transaction-feature-programatically)
