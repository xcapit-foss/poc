# Xcapit Chainlink Node + Oracle

WIP...

## Community

- [Discord](https://discord.gg/AnGXcZ8P)
- [Project Charter](https://xcapit-foss.gitlab.io/documentation/docs/project_charter)
- [Code of Conduct](https://xcapit-foss.gitlab.io/documentation/docs/CODE_OF_CONDUCT)
- [Contribution Guideline](https://xcapit-foss.gitlab.io/documentation/docs/contribution_guidelines)

## Getting Started
------

### Installation

Clone the repo and open the directory:

```sh
git clone https://gitlab.com/xcapit/dev-team/chainlink-node-poc.git xcapit-chainlink-node
cd xcapit-chainlink-node
```

### Configuration
Sample configuration files are available to quickly set up the service. To use them:

```sh
cp ./node/.credentials.example ./node/.credentials
cp ./node/.password.example ./node/.password
cp ./.env.example ./.env
cp ./database.env.example ./database.env
```
To customize them, check [configuration files](#configuration-files)
### Run Service

You can run Xcapit Chainlink Node with [Docker](https://www.docker.com/) by running the following commands.

```sh
docker-compose up -d
```
Open [Chainlink Operator Web UI](http://localhost:6688/) to configure or interact with the node. The credentials to access the Web UI are in the .credentials configuration file.


## Configuration Files
------
For configuration settings you could see and change the next files.

```sh
./node/.credentials
./node/.password
./.env
./database.env
```

### **.credentials**
These are the credentials to access the node's Web UI. It is a plain text file with the user and password with a line break in between. The password must be at least 16 characters long.

Example file:
```
user
supersecretpassword
```

### **.password**
This is the password of the postgres user we use. It is a plain text file that only has this password. The password must be at least 16 characters long.

Example file:
```
supersecretpassword
```

### **database.env**
Contains the variables that postgres will use to run the container. For consistency, we must set a password that is at least 16 characters long.

Example file:
```
POSTGRES_USER=postgres
POSTGRES_PASSWORD=supersecretpassword
POSTGRES_DB=chainlink_node_db
POSTGRES_PORT=5432
POSTGRES_HOST=postgres-chainlink-node
```

### **.env**
Contains the variables that configure the chainlink node

Example file:
```
LOG_LEVEL=debug
ETH_CHAIN_ID=5 # Chain ID of the network, in this case Goerli
CHAINLINK_TLS_PORT=0
SECURE_COOKIES=false
ALLOW_ORIGINS=*
ETH_URL=wss://eth-goerli.g.alchemy.com/v2/<ALCHEMY_KEY>
DATABASE_URL=postgresql://postgres:password@xcapit-chainlink-node_postgres-chainlink-node_1:5432/chainlink_node_db?sslmode=disable
```

For more information on how to customize this configuration, visit [the Chainlink documentation](https://docs.chain.link/chainlink-nodes/v1/configuration).

